#ifndef FONT5X7_H
#define FONT5X7_H

#include <ProgMemFont.h>

class Font5x7: public ProgMemFont {
	
private:

	static const uint8_t data[];
	
	static Font5x7* instance;

	Font5x7();
	

public:

	static Font5x7& getInstance();

};


#endif //FONT5X7_H
